module "vpc" {
  source = "git::https://gitlab.com/modules4/aws-vpc.git"
  
  vpc_name = "Network_VPC"
  vpc_cidr = "10.0.0.0/19"
}

